
\chapter{Introduction}
\label{chap:introduction}

%-------------------------------------------------------------------------------
\section{Starting Point}

A few kilobytes of RAM and non-volatile storage at most 1 MB are prominent restrictions when working with a microcontroller on an embedded system. Additionally, the clock most certainly is below 200 MHz, which means that the firmware must be efficient. Predominantly C or sometimes C++ is used as the programming language of choice. Another aspect of embedded systems is their reliability. An electronic locking system runs for years without a reset and still has to perform its task without failure. The firmware therefore must be deterministic and memory safe. This is where C passes the responsibility to the user. Void pointer casting, pointer management and memory freeing are error-prone, leading to \emph{runtime errors}. These errors are expensive to find and fix.

The solution for many of these problems might lie in a fairly new programming language. Rust is a system programming language focused on memory-safety and thread-safety at \emph{compile time} \cite{rust:lang}. Removing the chance for data races and a strong type system help to reduce runtime errors. Because Rust enforces its rules at compile time, no overhead is added at runtime, which results in the same performance as code written in C/C++. Thread-safety might seem irrelevant on a single-core microcontroller but comes in use whenever an interrupt service routine is called.

With the increasing complexity of an embedded system application, it becomes difficult to manage all tasks and resources. A real-time operating system (RTOS) helps organize and synchronize them. It also allows scheduling tasks to a specific time in the future deterministically.

As the number of native Rust RTOS is fairly limited, there is an opportunity to develop a new Rust RTOS with a new concept. \cite{luethi:pa1}

%-------------------------------------------------------------------------------
\section{Previous Work}

In the first of two preceding projects \cite{luethi:pa1} a comparison of established RTOS written in the C language revealed that there is a division into general purpose RTOS (\ucos-III, FreeRTOS) and IoT-centric RTOS (Contiki, Mynewt, Zephyr), although the border is blurry. In recent years, the orientation of RTOS has shifted from general purpose to internet of things (IoT). This was indicated by the increasing number of network stacks and support for cloud infrastructure integrated in RTOS. The work featured insight into \ucos-III, an extensively documented RTOS similar to FreeRTOS. Furthermore, the tools and structure of the Zephyr project from the Linux Foundation were reviewed.

As a result, it was decided that the Bern RTOS should implement similar features to those of \ucos-III and FreeRTOS. Network stacks and hardware access will not be integrated into the RTOS but rather offered as separate modules at some point. A first draft of the system architecture and design ideas were created.

Besides the RTOS comparison, an introduction into the Rust programming language was provided in the work. It became evident that Rust is indeed a suitable language for embedded systems because modern programming paradigms (e.g. generics, traits, closures) allow for abstraction while being efficient. Memory-safety and thread-safety guaranteed by the Rust compiler increase the safety and security of  embedded systems, especially if dynamic memory allocation is used. Many software bugs are already caught at compile time and safe hours of debugging on the hardware.

The second project consisted of software requirements specification and the implementation of core kernel components. The specifications compromise the whole RTOS. It focuses on memory-safety, timeliness and the overall feature set.

Based on the specifications, the architecture was adapted from the previous project and a first revision of the preemptive scheduler was implemented for the Armv7E-M CPU architecture. The kernel was based on a widespread task model. However, every task was limited to either access its own stack, a shared memory section or peripherals. That approach also prevents data corruption from a stack overflow. In order for multiple tasks to work together, the synchronization primitives mutex and semaphore were added to the kernel. The primitives had to be allocated statically in the shared memory section.

The correct behavior of the kernel was tested using a three-level system. Individual units are tested in insolation, architecture-dependent integration is tested against the target hardware and system-level tests are conducted to evaluate real-time capabilities.

At the end of the project, it became evident that the memory protection concept was flawed. In part because the memory protection unit of Armv7E-M based microcontrollers is too restrictive. Additionally, keeping all shared data in one globally accessible section meant that shared data cannot be trusted. One task might manipulate or corrupt all shared data. Therefore, the memory protection model must be changed.

This document is based on the second project documentation. Its contents have been reworked and extended.

%-------------------------------------------------------------------------------
\section{Objectives}

The main objective of the Bern RTOS project is to write a \emph{fail-safe} RTOS from the ground up. As many software bugs as possible should be caught at compile time to increase system stability and to avoid frustrating debugging sessions. Rust promises to achieve this goal, but we will have to figure out how to use the language effectively. A compiler cannot catch every error, therefore, we will need protection at runtime to keep a bug from crashing the whole system.

Developing an RTOS and learning Rust is a big challenge and will take up most of my Master of Science in Engineering (MSE) study. The development is taking place over two years and is split into three parts: a first project (9 ECTS), a second project (15 ECTS) and a master thesis (27 ECTS).

The goal of this thesis project is to extend the kernels features and to implement a complex example application. This leads to the following 3 objectives:
\begin{enumerate}
    \item The kernel should be improved based on the findings of the previous projects. This includes adding message passing, system logs, interrupt handling and dynamic memory allocation.
    
    \item The RTOS will be put the test on real-world use case: an espresso machine. The scenario showcases timelines with a periodic temperature control loop together with a high background load from a graphical user interface and an ethernet connection.

    \item The kernel internals and outstanding issues will be documented extensively so that development can continue as an open source project. 
\end{enumerate}


%-------------------------------------------------------------------------------
\section{Document Structure}

This document applies a top-down approach and is split into two parts. In the first part, an overview of the Bern RTOS and design principle is provided. Then follows a getting started guide introducing the kernel features based on an example application. Next are further details and the internal operation of the kernel explained. After that,  the testing strategy and framework are described. A discussion of the current implementation follows. 

In the second part, Bern RTOS is put the test on an espresso machine application. This part starts with an explanation of the theory of operation, the electronics developed and necessary hardware modification for the application. Then follows the documentation of the Bern RTOS based firmware. The temperature control method is presented. This part finishes with the results and discussion of the firmware and overall machine performance.

Finally, the conclusions over the entire project are drawn and recommendations for future work are given.

%-------------------------------------------------------------------------------
\section{Personal Motivation}

I have been programming firmware for microcontrollers with C or modern C++ bare-metal or using FreeRTOS. For example, a few colleagues and I used FreeRTOS on an omni-wheeled robot to control three motors from a speed vector, keeping track of its position and speed, displaying orientation on an LED strip and simultaneously interfacing a virtual COM port over USB. The RTOS allowed for easy synchronization and communication between the tasks. On the other hand, some obscure issues emerged, some from casting a void pointer into the wrong type, sharing the address of a variable that later went out of scope and stack overflows. Here I thought that there should be an RTOS that checks the types of messages and tasks parameters at compile time. And in particular, a stack overflow should not crash the system.

In contrast to C, C++ made development more convenient with classes, lists, iterators and generic code through templates. On the other hand, I have to carefully listen to the static analyzer and think of the implication on embedded devices from some features (e.g., virtual methods, polymorphism, RTTI, exceptions).  This is why Rust intrigues me: the language offers modern paradigms, but I do not have to worry about overhead in the assembly output. The drawback of Rust is that the language is still under development, so some features might just be missing altogether. \cite{luethi:pa1}


%-------------------------------------------------------------------------------
\section{Required Knowledge}

It is assumed that the reader has:
\begin{itemize}[nosep]
    \item programming experience
    \item knowledge of embedded systems (microcontrollers)
    \item basic knowledge of RTOS
    \item basic knowledge of Rust
\end{itemize}


