\chapter{Discussion}

As with any project, some design decisions were made at the beginning, but during implementation, issues emerged. In this chapter, design decisions and necessary adjustments are discussed.

%-------------------------------------------------------------------------------
\section{Process \& Thread Model}

Originally, Bern RTOS was meant to isolate every thread. Communication was only possible through system calls. The disadvantage was inefficient messaging with a high kernel involvement. But more importantly, an issue emerged from the memory protection in use. The memory protection unit (MPU) on Armv7E-M Cores (Cortex-M3/M4/M7) requires memory regions to follow $2^n$ byte size and alignment rule. For example, if 8 kB of Stack is not enough, the next larger size would be 16 kB. Some finer granularity can be achieved using subregion disable bits \cite[~373]{yiu_beckmann_2014}. However, that would increase the complexity of the alignment.

Instead, it was decided that multiple threads interacting with each other should share a common memory region. In accordance with general purpose operating systems, the naming adapted to processes and threads. The issue of memory size granularity still exists. However, as there are just a few processes instead of many threads, mitigating the effect of the issue.

%-------------------------------------------------------------------------------
\section{Kernel Stabilization}

When starting a new project, goals on the behavior and usage are set. Throughout development new learning impact these goals. Hence, the API, the internals and toolchain requirements change. Over time, breaking changes should subside and lead to a stable kernel.

%...............................................................................
\subsection{API}

The kernel API core elements are the same as in the first prototype. The API is based on existing Rust software, namely the standard library and tokio \cite{tokio:tokio} framework. An RTOS runs on resource-constrained devices. E.g., additional parameters such as stack size are required when spawning a thread on an embedded device. During the thesis, the usage of synchronization primitives was simplified by a reduction of method calls for initialization.

One ongoing problem is the unification of API calls for dynamically and statically allocated resources. At the moment, the API predominantly supports dynamic allocation only. One solution would be to move the allocation outside the kernel, like the thread stack, which has to be allocated and then passed to the kernel. This approach allows for the same methods to be used regardless of the allocation strategy. However, a problem are the management structures that are placed inside the kernel. For processes, a macro creates these structures and registers them in the kernel on initialization. Although this works, macros tend to break Rusts programming workflow because they do not allow for software interfaces.

The API could be improved by adding constraints on kernel calls depending on the context. For example, in an interrupt, we are not allowed to call a blocking lock method on a mutex. This mistake is caught at runtime and we receive an error. However, if there were traits for different contexts, the interrupt handler could be constrained to valid API calls. Again that would move another error source from runtime to compile time.

Decisions on how allocation should be handled will have the biggest impact on the API stabilization. Other adjustments, such as the constraints for contexts, should not impact the user. Similarly, thanks to the builder pattern, new parameters can be added without breaking the API.

%...............................................................................
\subsection{Internals}

The internals of the kernel (e.g. scheduler) are not noticeable to the user until something goes wrong. If there is an error in the context switch, the system crashed immediately. Thus, for a stable application, the kernel internals must be stable and well tested. For that reason testing should cover more scenarios than it currently does. Besides testing, the linter clippy \cite{github:clippy} should be introduced to the project to improve code quality and reduce software defects.

%...............................................................................
\subsection{Toolchain}

A requirement from the beginning was that Bern RTOS should compile on Rusts stable toolchain. It is expected that the target audience for the RTOS will only accept the stable toolchain. This requirement was relaxed to some degree during development. Nightly features such as inline assembly shortened iteration cycles compared to externally compiled assembly files. Over time, the inline assembly feature has been stabilized. Other nightly features were added only if there was progress on the stabilization, or if they can be replaced without much effort.

At the moment, only two nightly features are required. \mintinline{rust}{naked_functions} is used for inline assembly functions where the compiler should not stack registers i.e. context switch. This feature can be replaced with global inline assembly functions. Additionally, Bern RTOS requires the user to include the \mintinline{rust}{alloc} crate. The allocator itself needs an error function (\mintinline{rust}{default_alloc_error_handler}). In the future, the kernel should not allow for purely static memory allocation, making the \mintinline{rust}{alloc} crate optional. 

%-------------------------------------------------------------------------------
\section{Panic}

The kernel design prevents a software fault from spreading beyond a process boundary. If a thread violates the memory rules, it is terminated. A handler is called when the application panics. Keeping a fault contained can avert critical behavior, but it does not solve the larger problem. The system should recover from a fault. A common method is to trigger a system reset. Another option might be to restart the faulty process or thread. For the latter solution, peripheral initialization would have to be moved from the main function to a process initialization phase. For now a system reset is sufficient, but in the future the binding peripheral to process approach (similar to a microkernel) could be explored. 

%-------------------------------------------------------------------------------
\section{Static vs Dynamic Memory Allocation}

The first prototype of the kernel only allowed for statically allocated stacks. The intention of this was to start with the most stable and simple form of allocation. For a thread to be scheduled it needs a stack but also management structures. These structures are stored in kernel memory and inaccessible to the user. Hence, for every thread, a management structure is allocated in the kernel. The first software prototype could only a hold a given number of management structures. The amount of available structures is another RTOS parameter that a user has to set. To simplify the user interface it was decided to use dynamic allocation instead. The advantages are one less parameter to tune and errors can be caught at system boot. If all threads and IPC objects are created before the kernel is started, out of memory errors will occur during development and not after deployment.

Eventually, Bern RTOS should support both static and dynamic memory allocation. Critical threads could be set up using static allocation only, to guarantee memory at compile time. Threads with dynamic system loads may rely on other allocation methods.

Every process owns an allocator to prevent one thread from taking all memory from the system. On the other hand, this segments the available memory into  smaller pieces, which increases the risk of out of memory errors. Overall the safety from process isolation was prioritized. Additionally, selecting an allocator algorithm per process allows for finer customization for a given use case.

%-------------------------------------------------------------------------------
\section{Safety \& Security}

Rust memory-safety and thread-safety guarantees at compile time already provide an improvement in functional safety compared to C. Additionally, using traits and constraints, further requirements and checks can be added to the kernel API e.g., it is impossible for a single consumer to have multiple producers. Bern RTOS adds memory access guarantees (e.g. stack overflow prevention, process memory access) at runtime.

Some of the safety improvements also help with security. However, there is currently a loop hole in the system calls. Some calls copy data in kernel mode from one thread to another. The addresses and size are passed to the system call. Therefore, a thread can gain access to all memory by making a system call with a manipulated address and size. The system calls should be validated for security. The API could be adapted and range checks placed accordingly.

%-------------------------------------------------------------------------------
\section{Testing}

On the unit level, mostly standalone libraries are tested (i.e. queues, allocators). Components that are tightly bound together were often neglected. The most prominent issue is the event system. Channels and synchronization primitives can be tested, as long as they do not interact with the event system. To test these modules as well dependencies need to be weakened inside the kernel. One solution would be to introduce traits for components and a kernel builder. The builder could inject mocks for testing.

Between the last project and this thesis the system-level testing method was changed from instruction tracing to signals using pins. Using the Digilent Digital Discovery proved to be much more straightforward and more suitable for automation. Tracing still required manually evaluating instruction counts. Using a logic analyzer instead of an instruction tracing device is also more approachable because of the price difference. The test setup was used to evaluate interrupt latency and context switching time from a semaphore event. These limited tests prove that the method works and serve as a basis for further test cases.

The testing concept does not yet support the user in any way. As a first step, traits should be introduced for the complete kernel API. By mocking or stubbing kernel calls, the user could test individual threads. For an integration test of multiple threads or processes on the host system, a kernel emulation would be necessary. Though, the emulator is considered nice to have.
