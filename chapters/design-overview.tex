\chapter{Design Overview}
\label{chap:design-overview}

This chapter gives an overview of the objectives of the Bern RTOS and the design choices made to achieve these goals. 

%-------------------------------------------------------------------------------
\section{Design Principles}

Every real-time operating system (RTOS) is optimized for some use cases. Bern RTOS targets industrial connected embedded systems. These systems are subjected to dynamic load from network interfaces and must perform critical tasks on time. The RTOS is designed according to the following principles to meet the use cases.

%...............................................................................
\subsection{System Stability}

An embedded application should never crash. This cannot solely be achieved in the kernel. The user of the RTOS has some responsibility too. The kernel design, however, must never panic and should help the user avoid bugs. If one part of the software crashes, the kernel must prevent a complete system crash.

%...............................................................................
\subsection{Process \& Thread}

An embedded system handles many tasks at the same time. Not all of those tasks have the same criticality. Bern RTOS should allow any type of task to run on the same system. The user splits critical and other parts of the software into processes. Each process contains one or more threads that run similar to tasks on other RTOS. Processes run isolated from each other.

For example, a user can limit the effect of an attack on a network interface on the critical parts of the system or use an existing library written in C that the Rust compiler cannot verify.

%...............................................................................
\subsection{Flexibility}

Bern RTOS should be adaptable to any project by providing configuration options for all components of the RTOS.

The kernel should only rely on the CPU core and leave the microcontroller peripherals to the user. Thus, any hardware-abstraction layer (HAL) or none at all can be used. Because many microcontrollers use the same CPU architecture, the efforts for porting the kernel is often little to none.
    
%...............................................................................
\subsection{Round-Robin Preemptive Scheduling}

Most threads in embedded software can be strictly prioritized. Thus, high-priority threads should preempt lower priority ones. To share the CPU between threads of the same priority, threads should run for a maximum of one time unit, known as Round-Robin or time-slicing.

This scheduling approach is the default for many RTOS (FreeRTOS \cite{amazon:freertos}, Zephyr \cite{linux:zephyr}, Azure RTOS \cite{microsoft:azure-rtos}).
   
%...............................................................................
\subsection{Real-Time}

Microcontrollers are used to control hardware in real-time. The kernel overhead between an event on the hardware and executing the appropriate thread must be low. Repetitive threads are to run in a deterministic and timely manner.

If fast interrupt handling is crucial, the user must be able to bypass the kernel altogether. In a trade-off between safety and timeliness, the latter must be preferred.
   
%...............................................................................
\subsection{Stackful Threads}

Every thread has its own stack. By default, stacks should only be accessible from within a process. This prevents accidental or intentional manipulation of stack data from other processes. The compiler cannot foresee or prevent stack overflow. Therefore, the kernel must prevent corruption from overflow.

%-------------------------------------------------------------------------------
\section{Architecture}

\begin{figure}[htb!]
    \centering
    \includegraphics{architecture.pdf}
    \caption[Bern RTOS architecture]{Bern RTOS architecture; red: high-priority components, blue: lower priority components, green: not part of the Bern RTOS.}
    \label{fig:architecture}
\end{figure}

The Bern RTOS architecture in \autoref{fig:architecture} illustrates a clear separation of the kernel, which is based on the CPU, the hardware abstraction of the microcontroller peripherals and the isolation of the application.

The segmentation into these three parts serves as a basis for memory protection. Overall, the Bern RTOS applies thread privileges on memory segments and not on every single variable or GPIO.

%...............................................................................
\subsection{Kernel}

Bern RTOS comprises the kernel, drivers, services and tools. The core part is the kernel, which is responsible for scheduling and communication.

\subsubsection{Kernel API}

All public kernel functions a thread is allowed to access are present in the kernel application programming interface (API). The API also sets a barrier between threads running in user mode and the kernel in kernel mode. All kernel objects are protected and only accessible through the API.

\subsubsection{Kernel Components}

The kernel is built in a modular design, so that features can be added, configured by the user and modules can be replaced if needed. The kernel handles scheduling, inter-process communication (IPC), memory management and protection as well as logging. The scope of the features planned for the kernel is similar to FreeRTOS and \ucos-III.

\subsubsection{Architecture Dependent Code}

Most parts of a real-time kernel run on any CPU, e.g., scheduling the next thread. Thread switching and memory protection, on the other hand, are different for every CPU architecture. CPUs differ in the number of core registers, exception context, memory protection hardware and instruction set.

In order for the kernel to be portable, all architecture-dependent code is put into one crate (module). Because the kernel only uses the CPU and no microcontroller peripherals, this is only a small part of the kernel.

There is a large variety of microcontroller specialized in different use cases, but most of them use the same CPU architecture (e.g. Arm Cortex-M0+/3/4/7, RISC-V). Thus, once the kernel runs on one CPU, it is compatible with loads of microcontrollers.

%...............................................................................
\subsection{Hardware Abstraction Layer}

Device drivers and business logic can access the microcontroller peripherals via memory-mapped register. The issue here is that the registers are different for every microcontroller. A hardware abstraction layer (HAL) generalizes peripheral access with a common set of functions for a entire line of microcontrollers.

The Rust community is taking the HAL approach one step further with traits (similar to interfaces in C++) for common peripherals. The traits are then implemented for any microcontroller supported by the Rust community (\mintinline{rust}{embedded-hal} \cite{github:embedded-hal}). 

Bern RTOS will not implement a HAL because of two reasons:
\begin{enumerate}
    \item A HAL is implemented for every microcontroller. This is tedious work and leads to many half finished implementations instead of one commonly used HAL.
    \item Preexisting code using some HAL should be reusable. You should for example be able to start by writing a bare-metal application and then switch to an RTOS with minimal code changes.
\end{enumerate}

An application can access the HAL functions directly. Access control is enforced by enabling or locking the memory sections of memory-mapped registers.

%...............................................................................
\subsection{Applications}

An application compromises of a set of threads and communication. Typically, there is just one application running on a microcontroller. With memory isolation, Bern RTOS also allows multiple applications to run in parallel.

An application runs in user mode and has fewer privileges than the kernel.

Also planned to be part of the application space are stacks and services, e.g., TCP/IP, Bluetooth Low Energy (BLE) or FAT file system. These could be specific to Bern RTOS or third-party implementations.

%-------------------------------------------------------------------------------
\section{Memory Protection}

Memory is crucial concerning safety and security of an embedded system. 

\emph{Functional safety} is compromised if memory is manipulated \emph{by accident} leading to a malfunction of a critical part of the software. Issues regarding memory can arise when:
\begin{enumerate}[nosep]
    \item Data is passed between parts of the software and cast into the wrong type
    \item Data is written and read without synchronization (data race)
    \item Data is deallocated too early or multiple times (dangling pointer)
    \item Software reads or writes outside intended address spaces (overflow)
\end{enumerate}

Rust solves 1. with strong typing and generics, 2. by enforcing synchronization with \mintinline{rust}{Sync} traits and 3. with ownership and lifetime checks when compiling code.

4. is in part solved in the Rust language, as arrays and strings store their length with the data. Thus, copying strings will not write beyond bounds. Stack overflows, on the other hand, can still occur. One possible solution to reduce the risk of stack overflows is to use only a single stack. In the case of an RTOS, threads must either be cooperative (exiting the thread entry function every time) or hardware interrupts must be used for context switching, as in the RTIC real-time framework \cite{rtic}. Another solution is to detect a stack overflow by setting and checking watermark patterns or by trapping an overflow in hardware. 

\emph{Security} is compromised if an attacker can read or can manipulate data \emph{on purpose}. In microcontroller projects, security is often not regarded as a concern as these systems often run without connection to the outside world. With increased connectivity of embedded systems, a newly developed RTOS must have security in mind to avoid obsolescence in the coming years.

To prevent stack overflows and provide a secure system, Bern RTOS will rely on memory protection hardware. The main approach for safety and security is to form  processes by splitting the application by criticality. Bern RTOS has the following use cases in mind:

\begin{description}
    \item[Strict] Allocating everything statically is the safest option as memory is guaranteed to be available at compile time. The kernel should allow for threads, stacks and sync primitives to be allocated statically, so that they can run for sure. Threads should run in isolation and the kernel should copy data between threads. This is the safest and most secure option.
    
    \item[Relaxed] Making threads safe adds overhead at runtime, if a thread is less critical, it should be allocated in a fixed size pool of threads. To speed-up IPC, the kernel should provide a shared memory section where multiple threads have access to the same resource.
         
    \item[Dynamic] Allocating memory dynamically simplifies the handling of dynamic loads of communication interfaces. It is the most memory efficient solution because memory is reserved only if it is used. However, an overload of a communication interface might block the system by using all of its memory.
\end{description}

A combination of the three use cases above should be able to run on the same system.

On a microcontroller, flash memory, SRAM, peripherals and other memory components are mapped to a single address space. To isolate and protect parts, it must be placed into sections, as shown in \autoref{fig:memory-isolation}.

\begin{figure}[htb!]
    \centering
    \includegraphics{memory-isolation.pdf}
    \caption{Memory Protection: Memory Layout.}
    \label{fig:memory-isolation}
\end{figure}

Different threads have different access privileges to different sections. Insecure network stacks can, for example, be denied access to peripherals, except the network interface. If threads cannot access a memory section directly, they must ask the kernel for help via a system call (syscall).

The most secure system would allow every peripheral access via a syscall, so that the kernel can check the access permission with fine granularity. This is done in Tock OS \cite{tock-os} but implies high overhead and HAL implementation by the OS maintainers. As a compromise, peripherals in the Bern RTOS should be protected by either allowing full access to parts of the memory section or none. That way peripherals can be somewhat protected, but the kernel is still independent of a HAL and access is efficient.

In terms of flexibility, the aim is for the Bern RTOS to use some of the hardware memory protection capabilities to make the kernel safe and secure, and to leave some to the user to use for application-specific purposes. For example, a user might want to restrict access to encryption keys.

