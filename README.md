# Book - Bern RTOS Kernel

The latest version of this document is available as [PDF](https://gitlab.com/bern-rtos/doc/book-bern-rtos-kernel/-/jobs/artifacts/main/raw/Bern_RTOS_Kernel.pdf?job=latex) or as an [online book](https://kernel.bern-rtos.org/). Older version can be found the GitLab Release section.

## Prerequisites

For LaTeX (PDF):
- `pdflatex` (tex-full)
- `pygments`
- `make`

For mdBook (HTML):
- `mdbook`
- `python 3`
- Install the requirements to convert LaTeX to Markdown
  ```bash
  pip3 install -r mdbook/latex2md/requirements.txt
  ```

## Build

Update all submodules:
```bash
git submodule update --init
```

Build the PDF:
```bash
make release
```

Build the website:
```bash
./mdbook/convert.sh
```

The HTML files can be found in `./mdbook/book`.

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This <span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" rel="dct:type">work</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
